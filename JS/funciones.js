
const out = document.querySelector(".output");
let datos;

const obtener = () => {
    let url = `https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json`;
    return fetch(url).then(res => res.json());
}

window.addEventListener("load", async () => {
    
    datos = await obtener();
    let url = new URLSearchParams(location.search);
    let codigo = Number(url.get("codigo"));
    let valor = Number(url.get("nota"));
    estudiante = datos["estudiantes"].find(estudiante => estudiante.codigo == codigo);
    if(estudiante != null) {
        crearIntro(estudiante);
        let notas = estudiante["notas"].sort((a, b) => {
            let ok = 0;
             if(a.valor < b.valor) ok = -1;
             else if(a.valor > b.valor) ok = 1;
             return ok; 
        });
        cargarFunciones(notas, valor);
    }
    else {
        crearError("el código del estudiante");
    }

});

const crearIntro = (estudiante) => {
    const fragmento = document.createDocumentFragment();
    const item = document.createElement("DIV"); 
    const tabla = [["Nombre: ", estudiante["nombre"]], ["Código: ", estudiante["codigo"]], ["Materia: ", datos["nombreMateria"]]];
    for(let elemento of tabla) {
        const row = document.createElement("DIV"); 
        row.classList.add("row", "border", "border-1", "border-dark");
        
        row.innerHTML = `<div class="col-12 col-md-6">
                            <p>${elemento[0]}</p>
                        </div>
                         <div class="col-12 col-md-6">
                            <p>${elemento[1]}</p>
                         </div>
        `
        item.appendChild(row);
    }
    item.classList.add("container", "border", "border-1", "border-dark");
    
    out.appendChild(item);

}

const crearError = (msg) => {
    const fragmento = document.createDocumentFragment();
    const item = document.createElement("DIV"); 
    item.classList.add("container");
    item.classList.add("text-center");
    item.classList.add("p-3");
    item.innerHTML = ` <h1>Error al digitar ${msg}. Por favor vuelva a ingresar los datos.</h1>
    `;
    fragmento.appendChild(item);
    out.appendChild(fragmento);
};

const cargarFunciones = (notas, nota) => {

    let notasA = [];
    let notasDesaprobadas = 0, n = notas.length;
    for(let i = 0; i < n; i++) {
        let info, descripcion;
        text = "Nota Aprobada";
        if(i < nota) {
            text = "Nota Eliminada";
        }
        else if(notas[i].valor < 3) {
            text = "Nota Desaprobada";
            notasDesaprobadas++;
        }

        descripcion = datos["descripcion"][(notas[i].id)-1]["descripcion"];
        info = [descripcion, notas[i].valor, text];

        notasA.push(info);
    }

    //Tabla google
    let data = new google.visualization.DataTable();
    data.addColumn("string","Descripción");
    data.addColumn("number","Valor");
    data.addColumn("string","Observación");

    data.addRows(n+1);

    for(let i = 0; i < n; i++) {
        data.setCell(i, 0, notasA[i][0]);
        data.setCell(i, 1, notasA[i][1]);
        data.setCell(i, 2, notasA[i][2]);
    }

    data.setCell(n, 0, "NOTA DEFINITIVA");
    data.setCell(n, 1, calcularPromedio(notas, nota));
    data.setCell(n, 2, "NOTA DEFINITIVA");
    let table = new google.visualization.Table( document.querySelector(".tabla"));

    table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});

    //PIE

    data = new google.visualization.arrayToDataTable([
        ['Notas', 'Cantidadd'],
        ['Notas Aprobadas',     (notasA.length-nota) - notasDesaprobadas],
        ['Notas Desprobadas',      notasDesaprobadas]
      ]);
    
    let options = {
        title: 'Notas',
        is3D: true,
      };
    
    const divP = document.querySelector(".pie");
    divP.setAttribute("style", "width: 1000px; height: 500px;");
    
    let chart = new google.visualization.PieChart(divP);
    chart.draw(data, options);

};


const calcularPromedio = (notas, nota) => {
    if(nota == "null") nota = 0;
    let promedio = 0;
    for(let i = nota; i < notas.length; i++) {
        promedio += notas[i]["valor"];
    }
    promedio = promedio / notas.length;
    return promedio.toFixed(1);
}
