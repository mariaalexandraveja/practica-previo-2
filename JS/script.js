const botonAutoevaluacion = document.querySelector(".autoevaluacion");
let datos;

const obtener = () => {
    let url = `https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json`;
    return fetch(url).then(res => res.json());
}

botonAutoevaluacion.addEventListener("click", async () => {
    valor = parseInt(document.querySelector("#nota").value);
    codigo = parseInt(document.querySelector("#codigo").value);
    datos = await obtener();
    estudiante = datos["estudiantes"].find(estudiante => estudiante.codigo == codigo);
    if(estudiante != null) {
        let notas = estudiante["notas"].sort((a, b) => {
            let ok = 0;
             if(a.valor < b.valor) ok = -1;
             else if(a.valor > b.valor) ok = 1;
             return ok; 
        });
        let promedio = calcularPromedio(notas, valor);
        alert(`Mi nota es de : ${promedio} \nEsta nota es debido a: Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum veritatis tempora, atque aliquam molestiae reprehenderit rerum libero, rem, assumenda nulla harum. Esse sunt ut doloremque.`);
    }
    else {
        alert("Ingrese el código del estudiante a consultar");
    }
});

const calcularPromedio = (notas, nota) => {
    if(nota == "null") nota = 0;
    let promedio = 0;
    for(let i = nota; i < notas.length; i++) {
        promedio += notas[i]["valor"];
    }
    promedio = promedio / notas.length;
    return promedio.toFixed(1);
}
